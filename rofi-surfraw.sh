#!/usr/bin/env bash

# if the input is quit
if [ x"$@" = x"quit" ]
then
    exit 0
fi

elviList=($(sr -elvi | awk '{if (NR != 1) print $1}'))

# if there is no input
if [ -z "$@" ]
then
    # first step
    # list all the elvi in rofi
    for i in "${elviList[@]}"
    do
        echo $i
    done
else
    # test if first input is an elvi
    isElvi=false
    firstWord=$(echo $@ | awk '{print $1}')
    for i in "${elviList[@]}"
    do
        if [ $i == $firstWord ]; then
            isElvi=true
        fi
    done

    # if the first input is an elvi
    if [ $isElvi == true ]; then
        coproc( sr $@ )
        exit 0
    else
        # if the first input is not an elvi
        # launch sr with default elvi
        coproc( sr "S" $@ )
        exit 0
    fi
fi
# list all the elvi in rofi
# 1. user selects elvi but types no query
# 2. user types query but no elvi
# 3. user selects elvi and types query
