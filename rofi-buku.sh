#!/usr/bin/env bash

if [ "$@" ]
then
    #echo opening $@
    #echo $(echo $@ | awk '{print $1;}')
    buku -o "$(echo $@ | awk '{print $1;}')" n<$-
else
    buku --np -p -f 3 | awk '{if (NR != 1) print }'
fi

