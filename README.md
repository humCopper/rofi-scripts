# Rofi Script  

Scripts for [rofi](https://davedavenport.github.io/rofi/).  

## rofi-surfraw.sh  
Use rofi to query surfraw.
> rofi -modi Search:/path/to/rofi-surfraw.sh -show Search

## rofi-buku.sh
Use rofi to search buku.
> rofi -modi Bookmark:/path/to/rofi-buku.sh -show Bookmark
